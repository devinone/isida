from django.conf.urls import url, include
# from django.utils.translation import ugettext as _
from home import views
from django.views.generic import RedirectView


urlpatterns = [
    url(r'^$',views.home,name='home'),
    # url(r'^FAQ/$', views.getting_started, name='getting_started'),
    # url(r'^about/$', views.about, name='about'),
    url(r'^403/$', views.page403, name='page403'),
    url(r'^404/$', views.page404, name='page404'),
    url(r'^500/$', views.page500, name='page500'),
    url(r'^503/$', views.page503, name='page503'),
]
