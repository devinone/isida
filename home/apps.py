from django.apps import AppConfig
from django.db import TypeCoin


class HomeConfig(AppConfig):
    name = 'home'
