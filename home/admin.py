from django.contrib import admin
from wallet.models import Wallet

# admin.site.register(Wallet)

@admin.register(Wallet)
class WalletAdmin(admin.ModelAdmin):
    list_display = ['user','wallet_addres_btc','wallet_addres_dash','wallet_addres_lite','is_active']
