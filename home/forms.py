from django import forms

class WalletCheckForm(forms.Form):
    coin = forms.CharField(max_length=4)
    wallet = forms.CharField(label='Your wallet', max_length=45)


    def clean_coin_wallet(self):
        coin = self.cleaned_data.get('coin')
        wallet = self.cleaned_data.get('wallet')
        return coin, wallet
    # def clean_email(self):
    #     email = self.cleaned_data.get('wallet')
    #     if email and User.objects.filter(email=email).exclude(username=username).exists():
    #         raise forms.ValidationError("Wallet isn't register.")
    #     return email
