import time, json, requests
from django.shortcuts import render, HttpResponse
from django.http import HttpResponseRedirect, JsonResponse
# from django.utils.translation import activate
from .forms import WalletCheckForm
from api import views as api
from coin.models import COIN
"""import modules"""
# import datetime
# Create your views here.

def home(request):
    try:
        owl = COIN.objects.get(name="owl")
        owl.get_difftime()
        owl.update_coins()
        owl.save()
    except COIN.DoesNotExist:
        owl = COIN.objects.create(name = "owl")
        owl.activate()
        owl.save()

    owl.get_difftime()
    owl.update_coins()
    owl.save()
    if request.method == 'POST':
        form = WalletCheckForm(request.POST)
        if form.is_valid():
            form = WalletCheckForm()
            coin, wallet = form.clean_coin_wallet()
            blocks = api.loadLastFoundedBlocks('europe1',"dash")
            output = api.loadHomePagePoolInfo('europe1',coin)###
            output_private = api.loadHomePagePrivateInfo('europe1',coin, wallet)
            return render(request,'home/index.html', {'output':output,"output_private":output_private,"form":form,"blocks":blocks})
    else:
        form = WalletCheckForm()
        blocks = api.loadLastFoundedBlocks('europe1',"dash")
        output = api.loadHomePagePoolInfo('europe1',"dash")
        output_private = api.loadHomePagePrivateInfo('europe1',"dash", "wallet")
    return render(request,'home/index.html', {'output':output,"output_private":output_private,"form":form,"server":"europe1","blocks":blocks})


def getting_started(request):
    return render(request,'help/getting_started.html')

def about(request):
    return render(request,'help/about.html')

def update_wallets(request):
    return render(request,'home/wallets.html')

def home_files(request,filename):
    if filename=='robots.txt':
        return render(request,'robots.txt')
    elif filename=='humans.txt':
        return render(request,'humans.txt')

def page403(request):
    return render(request,'errors/403.html')

def page404(request):
    return render(request,'errors/404.html')

def page500(request):
    return render(request,'errors/500.html')

def page503(request):
    return render(request,'errors/500.html')
