from django.db import models
from jsonfield import JSONField
from datetime import datetime, timedelta
from django.utils.timezone import utc
import time, pytz, json, requests


class COIN(models.Model):
    name = models.CharField(max_length=4)
    COIN1 = JSONField()
    COIN4 = JSONField()
    COIN34 = JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_updated = models.BooleanField(default = True)

    class Meta:
        verbose_name="Coin info"
        verbose_name_plural="Coins info"

    def activate(self):
        self.COIN1 = {"id":1,"name":"qwe","tag":"BTC","algorithm":"SHA-256","block_time":"655.0","block_reward":12.7302,"block_reward24":12.7131686868687,"block_reward3":12.6798465515369,"block_reward7":12.7153858815636,"last_block":514431,"difficulty":3462542391191.6,"difficulty24":3462542391191.6,
        "difficulty3":3437583881160.48,"difficulty7":3360694713315.15,"nethash":22704589818597801526,"exchange_rate":8984.9,"exchange_rate24":8583.89809215263,"exchange_rate3":8084.34330686134,"exchange_rate7":8126.06395247372,"exchange_rate_vol":55677.32595972,"exchange_rate_curr":"BTC","market_cap":"$152,116,785,412","pool_fee":"0.000000","estimated_rewards":"0.001035","btc_revenue":"0.00103543","revenue":"$9.30",
        "cost":"$3.29","profit":"$6.02","status":"Active","lagging":False,"timestamp":1521583218}

        self.COIN4 = {"id":34,"name":"Dash","tag":"DASH","algorithm":"X11","block_time":"159.0","block_reward":1.8014759475218658,"block_reward24":1.80147594752187,"block_reward3":1.80147594752187,"block_reward7":1.80147594752187,"last_block":839878,"difficulty":73764874.865325,"difficulty24":78283890.2937219,"difficulty3":70822227.8920461,"difficulty7":75207897.7997629,
        "nethash":1992564309057253, "exchange_rate":0.049182,"exchange_rate24":0.0480158903318904,"exchange_rate3":0.0473842082642249,"exchange_rate7":0.0487637907523616,"exchange_rate_vol":1951.912819835,"exchange_rate_curr":"BTC","market_cap":"$3,516,596,095","pool_fee":"0.000000","estimated_rewards":"0.006944","btc_revenue":"0.00034151","revenue":"$3.07","cost":"$2.88","profit":"$0.19",
        "status":"Active","lagging":False,"timestamp":1521583267}

        self.COIN34 = {"id":4,"name":"Litecoin","tag":"LTC","algorithm":"Scrypt","block_time":"154.0","block_reward":25.0,"block_reward24":25.0,"block_reward3":25.0,"block_reward7":25.0,"last_block":1388742,"difficulty":5771138.982,"difficulty24":5771138.98200004,"difficulty3":5771138.98200001,"difficulty7":5721898.49855023,
        "nethash":160953592132212,"exchange_rate":0.018953,"exchange_rate24":0.0188282356321839,"exchange_rate3":0.018892645709524,"exchange_rate7":0.019178216369736,"exchange_rate_vol":2241.7360078,"exchange_rate_curr":"BTC","market_cap":"$9,487,901,708","pool_fee":"0.000000","estimated_rewards":"0.043571","btc_revenue":"0.00082581",
        "revenue":"$7.42","cost":"$1.92","profit":"$5.50","status":"Active","lagging":False,"timestamp":1521583271}
        self.save()


    def get_difftime(self):
        if self.updated_at:
            now = datetime.utcnow().replace(tzinfo=pytz.utc)
            timediff = now - self.updated_at
            if timediff.total_seconds() > 100:
                self.is_updated = False
                self.save()

    def update_coins(self):
        if (self.is_updated==False):
            try:
                url = 'https://whattomine.com/coins/1.json'
                self.COIN1 = requests.get(url).json()
                time.sleep(1)
                url = 'https://whattomine.com/coins/4.json'
                self.COIN4 = requests.get(url).json()
                time.sleep(1)
                url = 'https://whattomine.com/coins/34.json'
                self.COIN34 = requests.get(url).json()
                self.is_updated = True
                self.save()
            except:
                print("Can't update coin stats, try again")


    def __str__(self):
                return '%s %s %s' % (self.name, self.updated_at, self.is_updated)


    # Create your models here.
