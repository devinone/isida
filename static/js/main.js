var chartPoolrateDate,chartPayoutsDate,currentHash;
var timePeriod = 'month';
var startChartPoolrate = true;
var startChartPayouts = true;

document.addEventListener("DOMContentLoaded", function(event){
  if(window.innerWidth>959){
    var heightHeader = document.querySelectorAll('.main-container>header')[0].offsetHeight;
    var heightFooter = document.querySelectorAll('.main-container>footer')[0].offsetHeight;
    var heightFinal = (window.innerHeight - heightHeader - heightFooter) / 2 - 75;
    document.querySelectorAll('.chartjs-wrapper')[0].style.height = heightFinal + 'px';
    document.querySelectorAll('.chartjs-wrapper')[1].style.height = heightFinal + 'px';
  }
});

function setDataChart(){
  $.ajax({
      url: '/en/event/' + localStorage.getItem("current-server") + '/pool/' + localStorage.getItem("current-coin") + '/' + timePeriod + '/',
      type: 'GET',
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      success: function(result){
        if (result.success==='true'){
          chartPoolrateDate = {
            labels: result.data_time,
            datasets: [{
                label: 'Good',
                backgroundColor: 'rgb(75, 192, 192)',
                data: result.data_power_good
            }, {
                label: 'Orphan',
                backgroundColor: 'rgb(54, 162, 235)',
                data: result.data_power_orphan
            }, {
                label: 'Doa',
                backgroundColor: 'rgb(255, 99, 132)',
                data: result.data_power_doa
            }]
          };
          if (startChartPoolrate){
            startChartPoolrate = false;
            currentHash = result.dimension_hash_rate;
            runChartPoolrate();
          } else {
            chartPoolrate["config"]["data"] = chartPoolrateDate;
            chartPoolrate["config"]["options"]["title"]["text"] = "Pool rate " + localStorage.getItem("current-coin").toUpperCase() + " " + result.dimension_hash_rate + " (+00:00 UTC)";
            window.chartPoolrate.update();
          };
          $('#chart-poolrate').css('filter','unset');
          $('#chart-poolrate').closest('div').children('.error').not('.uk-hidden').addClass('uk-hidden');
        } else {
          chartPoolrateDate = {
            labels: ['0','1','2'],
            datasets: [{
                label: 'Good',
                backgroundColor: 'rgb(75, 192, 192)',
                data: ['0','1','2'],
            }, {
                label: 'Orphan',
                backgroundColor: 'rgb(54, 162, 235)',
                data: ['0','1','2'],
            }, {
                label: 'Doa',
                backgroundColor: 'rgb(255, 99, 132)',
                data: ['0','1','2'],
            }]
          };
          if (startChartPoolrate){
            startChartPoolrate = false;
            runChartPoolrate();
          };
          $('#chart-poolrate').css({'filter':'blur(2px)','-webkit-filter':'blur(5px)','-moz-filter':'blur(5px)','-ms-filter':'blur(5px)'});
          $('#chart-poolrate').closest('div').children('.error').removeClass('uk-hidden');
        }
      }
  });
  $.ajax({
      url: '/en/event/' + localStorage.getItem("current-server") + '/payouts/' + localStorage.getItem("current-coin") + '/' + timePeriod + '/',
      type: 'GET',
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      success: function(result){
        if (result.success==='true'){
          chartPayoutsDate = {
            labels: result.data_time,
            datasets: [{
                label: 'Coins',
                backgroundColor: 'rgb(54, 162, 235)',
                data: result.data_payouts
            }]
          };
          if (startChartPayouts){
            startChartPayouts = false;
            runChartPayouts();
          } else {
            chartPayouts["config"]["data"] = chartPayoutsDate;
            chartPayouts["config"]["options"]["title"]["text"] = "Current pool payouts " + localStorage.getItem("current-coin").toUpperCase() + " (+00:00 UTC)";
            window.chartPayouts.update();
          };
          $('#chart-poolpayouts').css('filter','unset');
          $('#chart-poolpayouts').closest('div').children('.error').not('.uk-hidden').addClass('uk-hidden');
        } else {
          chartPayoutsDate = {
            labels: ['0','1','2','0','1','2'],
            datasets: [{
                label: 'Coins',
                backgroundColor: 'rgb(54, 162, 235)',
                data: ['0','1','2','0','1','2']
            }]
          };
          if (startChartPayouts){
            startChartPayouts = false;
            runChartPayouts();
          };
          $('#chart-poolpayouts').css({'filter':'blur(2px)','-webkit-filter':'blur(5px)','-moz-filter':'blur(5px)','-ms-filter':'blur(5px)'});
          $('#chart-poolpayouts').closest('div').children('.error').removeClass('uk-hidden');
        }
      }
  });
};
function setDataPoolStats(){
  $.ajax({
    url: '/en/event/' + localStorage.getItem("current-server") + '/info/' + localStorage.getItem("current-coin") + '/',
    type: 'GET',
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    success: function(result){
      if (result.success==='true'){
        $('#stat-hashrate').children('.green').html(result.effective_local_pool_hashrate);
        $('#stat-hashrate').children('.red').html(result.dead_local_pool_hashrate);
        $('#stat-hashrate').children('.dimension').html(result.dimension_hash_rate);
        $('#stat-efficiency').html(result.pool_efficiency + '%');
        $('#stat-workers').html(result.number_of_workers);
        $('#stat-shares').children('.green').html(result.shares_total);
        $('#stat-shares').children('.red').html(result.shares_orphan);
        $('#stat-shares').children('.gray').html(result.shares_dead);
        $('#stat-sharetime').html(result.time_to_share);
        $('#stat-timetoblock').html(result.time_to_block);
        $('#stat-diff').html(result.current_ave_difficulty);
        $('#stat-uptime').html(result.uptime);
        if (!startChartPayouts){
          UIkit.notification({ message: "Pool stats downloaded", status: "success", pos: "bottom-left", timeout: 2000 });
        };
        $('.see-stats-anon .uk-button').removeAttr('disabled');
      } else {
        $('#stat-hashrate').children('.green').html('0');
        $('#stat-hashrate').children('.red').html('0');
        $('#stat-efficiency').html('0%');
        $('#stat-workers').html('0');
        $('#stat-shares').children('.green').html('0');
        $('#stat-shares').children('.red').html('0');
        $('#stat-shares').children('.gray').html('0');
        $('#stat-sharetime').html('infinity');
        $('#stat-timetoblock').html('infinity');
        $('#stat-diff').html('0');
        $('#stat-uptime').html('0 days');
        $('.see-stats-anon .uk-button').attr('disabled','disabled');
      }
    }
  });
};
function setDataPoolStatsAnon(){
  $.ajax({
    headers: { "X-CSRFToken": $('#csrf input').val() },
    url: '/en/event/' + localStorage.getItem("current-server") + '/info/private/',
    type: 'POST',
    cache: "false",
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    data: JSON.stringify({ coin: localStorage.getItem("current-coin"), wallet: $("#id_wallet").val() }),
    success: function(result){
      $('#mystat-hashrate').html(result.private_hashrate);
      $('#mystat-dimension').html(result.dimension_hash_rate);
      $('#mystat-reward24').html(result.private_rewards24);
      $('#mystat-reward24dollar').html(result.private_rewards24_dollar + ' $');
      $('#mystat-reward30d').html(result.private_rewards30d);
      $('#mystat-reward30dollar').html(result.private_rewards30d_dollar + ' $');
      $('#mystat-updated').html(result.updated);
      if (result.success==='true'){
        UIkit.notification({ message: "Stats downloaded", status: "success", pos: "bottom-left", timeout: 2000 });
      } else {
        UIkit.notification({ message: "Oups! didn't find worker with wallet:<br/><span class='uk-text-small'>" + $("#id_wallet").val() + "</span>", status: "warning", pos: "bottom-left", timeout: 2000 });
      }
    }
  });
};
function setFoundBlocks(){
  $.ajax({
    url: '/en/event/' + localStorage.getItem("current-server") + '/blocks/' + localStorage.getItem("current-coin") + '/',
    type: 'GET',
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    success: function(result){
      $('.found-blocks tbody').html('');
      if (result.success==='true'){
        $.each(result.blocks, function(i, item) {
          var $tr = $('<tr>').append(
              $('<td>').text(item.block_number),
              $('<td>').text(item.block_mined),
              $('<td>').text(item.block_hash),
              $('<td>').html('<a href="' + item.url + '">url to block</a>')
          ).appendTo('.found-blocks tbody');
        });
      } else {
        $('<tr>').append(
          $('<td>').text('Not founded blocks yet'),
          $('<td>').text('Not founded blocks yet'),
          $('<td>').text('Not founded blocks yet'),
          $('<td>').text('Not founded blocks yet'),
        ).appendTo('.found-blocks tbody');
      }
    }
  });
};
function runChartPoolrate(){
  window.chartPoolrate = new Chart(document.getElementById("chart-poolrate").getContext("2d"), {
    type: 'bar',
    data: chartPoolrateDate,
    ticks: {
      min: -3,
      max: 3,
      stepSize: 3,
      reverse: true,
    },
    options: {
      title:{ display:true, fontSize: 15, fontFamily: "Raleway", text:"Pool rate " + localStorage.getItem("current-coin").toUpperCase()  + " " + currentHash + " (+00:00 UTC)"},
      tooltips: { mode: 'index', intersect: false },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          stacked: true,
          ticks: { maxTicksLimit: 7,maxRotation: chartRotation },
        }],
        yAxes: [{ stacked: true, beginAtZero:true }]
      }
    }
  });
};
function runChartPayouts(){
  window.chartPayouts = new Chart(document.getElementById("chart-poolpayouts").getContext("2d"), {
    type: 'bar',
    data: chartPayoutsDate,
    ticks: {
      min: -3,
      max: 3,
      stepSize: 3,
      reverse: true,
    },
    options: {
      title:{ display:true, fontSize: 15, fontFamily: "Raleway", text:"Current pool payouts " + localStorage.getItem("current-coin").toUpperCase() + " (+00:00 UTC)" },
      tooltips: { mode: 'index', intersect: false },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          stacked: true,
          ticks: { maxTicksLimit: 7,maxRotation: chartRotation },
        }],
        yAxes: [{ stacked: true, beginAtZero:true }]
      }
    }
  });
};

$(document).ready(function(){
  setDataPoolStats();
  setFoundBlocks();
  $(".choose-coin a").click(function(e){
    if (!$(this).hasClass('disabled')){
      e.preventDefault();
      setDataChart();
      setDataPoolStats();
      setFoundBlocks();
      $(".choose-coin li").removeClass("uk-active");
      $(this).parent().addClass("uk-active");
      $('#id_coin').val(localStorage.getItem("current-coin"));
      $('#id_wallet').val();
    }
  });
  $(".choose-time a").click(function(e){
    e.preventDefault();
    if (!$(this).hasClass('disabled')){
      runBockerButton($(this));
      timePeriod=$(this).data('lang');
      setDataChart();
      $(".choose-time a").removeClass("uk-active");
      $(this).addClass("uk-active");
    } else {
      showForbiddenMessage();
    }
  });

  $(".see-stats-anon .uk-button").click(function(e){
    e.preventDefault();
    if (!$(this).hasClass('disabled')){
      runBockerButton($(this));
      setDataPoolStatsAnon();
    } else {
      showForbiddenMessage();
    }
  });

  setTimeout(function(){
    $('.chartjs-wrapper .uk-spinner').fadeOut();
    setDataChart();
  }, 600);
  setTimeout(function(){
    $('.chartjs-wrapper .uk-background-white').fadeOut();
  }, 1200);

  $(".choose-coin").find('a.' + localStorage.getItem('current-coin')).closest("li").addClass("uk-active");
  $('#id_coin').val(localStorage.getItem("current-coin"));
});

$(window).on("resize", function (){
  if(window.innerWidth>959){
    heightHeader = document.querySelectorAll('.main-container>header')[0].offsetHeight;
    heightFooter = document.querySelectorAll('.main-container>footer')[0].offsetHeight;
    heightFinal = (window.innerHeight - heightHeader - heightFooter) / 2 - 60;
    $('.main-container').find('.chartjs-wrapper:not(.uk-height-1-1)').css('height', heightFinal);
  } else {
    $('.main-container').find('.chartjs-wrapper:not(.uk-height-1-1)').css('height','200px');
  };
});
