var chartRotation;

document.addEventListener("DOMContentLoaded", function(event){
  if(window.innerWidth>959){
    document.querySelectorAll('.main-container>.uk-sticky-placeholder')[0].style.height = '90px';
    chartRotation = 0;
  } else {
    document.querySelectorAll('.main-container>.uk-sticky-placeholder')[0].style.height = '66px';
    chartRotation = 45;
  }
});

if(!localStorage.getItem("current-coin")){
  localStorage.setItem("current-coin", "dash");
};
if(!localStorage.getItem("current-server")){
  localStorage.setItem("current-server", "europe1");
};

function adaptive() {
  if(window.innerWidth>959){
    document.querySelectorAll('.main-container>.uk-sticky-placeholder')[0].style.height = '90px';
    setTimeout(function(){ UIkit.update(event = 'update'); }, 500);
  } else {
    document.querySelectorAll('.main-container>.uk-sticky-placeholder')[0].style.height = '66px';
  };
};
window.addEventListener("resize", adaptive);

function showForbiddenMessage(){
  UIkit.notification({ message: "Too often requested. Forbidden", status: "danger", pos: "bottom-left", timeout: 2000 });
};

function runBockerButton (button) {
  var timeOnTimer, timerObject, timerObjectText, timerAnon;
  timerObject = button;
  if (!timerObject.hasClass('disabled')) {
    timerObjectText = button.text();
    timeOnTimer = 10;
    timerAnon = setInterval((function() {
      timeOnTimer = timeOnTimer - 1;
      $(timerObject).addClass('uk-button-disabled disabled').attr('disabled',true).text(timeOnTimer + ' seconds');
      if (timeOnTimer <= 0) {
        clearInterval(timerAnon);
        $(timerObject).removeClass('uk-button-disabled disabled').removeAttr('disabled').text(timerObjectText);
      }
    }), 1000);
  } else {
    showForbiddenMessage();
  }
};

$(document).ready(function(){
  document.querySelector('#sidebar .choose-server>a span').textContent = localStorage.getItem("current-server");

  $(".choose-coin a").click(function(e){
    localStorage.setItem("current-coin", this.className);
  });
  $(".choose-server li a").click(function(){
    localStorage.setItem("current-server", this.className);
    window.location.reload();
  });
  $("#sidebar li.home a").click(function(){
    localStorage.setItem("current-server", "europe1");
  });
});