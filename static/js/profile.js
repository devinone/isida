var timePeriod = 'month';
var startChartPayoutsDonut = true;
var startChartPayouts = true;
var startChartPoolrate = true;
var chartPayoutsDate, btcDollar, dashDollar, ltcDollar, currentHash;

document.addEventListener("DOMContentLoaded", function(event){
  if(window.innerWidth>959){
    var heightHeader = document.querySelectorAll('.main-container>header')[0].offsetHeight;
    var heightFooter = document.querySelectorAll('.main-container>footer')[0].offsetHeight;
    var heightFinal = (window.innerHeight - heightHeader - heightFooter) / 2 - 65;
    document.querySelectorAll('.chartjs-wrapper')[1].style.height = heightFinal + 'px';
    document.querySelectorAll('.chartjs-wrapper')[2].style.height = heightFinal + 'px';
  }
});

function setDataChart(){
  $.ajax({
    url: '/en/event/' + localStorage.getItem("current-server") + '/profile/' + timePeriod + '/',
    type: 'GET',
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    success: function(result){
      if (result.success==='true'){
        btcDollar = result.output_rewards[0].private_rewards_dollar;
        dashDollar = result.output_rewards[1].private_rewards_dollar;
        ltcDollar = result.output_rewards[2].private_rewards_dollar;
        chartPayoutsDate = [btcDollar,dashDollar,ltcDollar];
        document.getElementById('btc-amount').textContent = result.output_rewards[0].private_rewards;
        document.getElementById('dash-amount').textContent = result.output_rewards[1].private_rewards;
        document.getElementById('ltc-amount').textContent = result.output_rewards[2].private_rewards;
        document.getElementById('btc-reward').textContent = result.output_rewards[0].private_rewards_dollar;
        document.getElementById('dash-reward').textContent = result.output_rewards[1].private_rewards_dollar;
        document.getElementById('ltc-reward').textContent = result.output_rewards[2].private_rewards_dollar;
        document.getElementById('total-reward').textContent = result.output_rewards[0].private_rewards_dollar + result.output_rewards[1].private_rewards_dollar + result.output_rewards[2].private_rewards_dollar;
        $('#chart-donut').css('filter','unset');
        $('#chart-donut').closest('div').children('.error').not('.uk-hidden').addClass('uk-hidden');
      } else {
        chartPayoutsDate = [1,2,3];
        $('#chart-donut').css({'filter':'blur(2px)','-webkit-filter':'blur(5px)','-moz-filter':'blur(5px)','-ms-filter':'blur(5px)'});
        $('#chart-donut').closest('div').children('.error').removeClass('uk-hidden');
      };
      if (startChartPayoutsDonut){
        startChartPayoutsDonut = false;
        runChartPayoutsDonut();
      } else {
        chartPayoutsDonut.data.datasets.forEach(function(dataset) {
          dataset.data = chartPayoutsDate;
        });
        window.chartPayoutsDonut.update();
      };
    }
  });
  $.ajax({
      url: '/en/event/' + localStorage.getItem("current-server") + '/profile/graph/' + localStorage.getItem("current-coin") + '/' + timePeriod + '/',
      type: 'GET',
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      success: function(result){
        if (result.success==='true'){
          chartPoolrateDate = {
            labels: result.output_total.data_time,
            datasets: [{
                label: 'Good',
                backgroundColor: 'rgb(75, 192, 192)',
                data: result.output_total.data_power_total
            }, {
                label: 'Orphan',
                backgroundColor: 'rgb(54, 162, 235)',
                data: result.output_dead.data_power_dead
            }]
          };
          if (startChartPoolrate){
            startChartPoolrate = false;
            currentHash = result.output_total.dimension_hash_rate;
            runChartPoolrate();
          } else {
            chartPoolrate["config"]["data"] = chartPoolrateDate;
            chartPoolrate["config"]["options"]["title"]["text"] = "Pool rate " + localStorage.getItem("current-coin").toUpperCase() + " " + result.output_total.dimension_hash_rate + " (+00:00 UTC)";
            window.chartPoolrate.update();
          };
          $('#chart-poolrate').css('filter','unset');
          $('#chart-poolrate').closest('div').children('.error').not('.uk-hidden').addClass('uk-hidden');
        } else {
          chartPoolrateDate = {
            labels: ['0','1','2'],
            datasets: [{
                label: 'Good',
                backgroundColor: 'rgb(75, 192, 192)',
                data: ['0','1','2'],
            }, {
                label: 'Orphan',
                backgroundColor: 'rgb(54, 162, 235)',
                data: ['0','1','2'],
            }]
          };
          if (startChartPoolrate){
            startChartPoolrate = false;
            runChartPoolrate();
          };
          $('#chart-poolrate').css({'filter':'blur(2px)','-webkit-filter':'blur(5px)','-moz-filter':'blur(5px)','-ms-filter':'blur(5px)'});
          $('#chart-poolrate').closest('div').children('.error').removeClass('uk-hidden');
        }
      }
  });
  $.ajax({
      url: '/en/event/' + localStorage.getItem("current-server") + '/profile/payouts/' + localStorage.getItem("current-coin") + '/' + timePeriod + '/',
      type: 'GET',
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      success: function(result){
        console.log(result)
        if (result.success==='true'){
          chartPayoutsDate = {
            labels: result.data_time,
            datasets: [{
                label: 'Coins',
                backgroundColor: 'rgb(54, 162, 235)',
                data: result.output_payouts.data_power_payouts
            }]
          };
          if (startChartPayouts){
            startChartPayouts = false;
            runChartPayouts();
          } else {
            chartPayouts["config"]["data"] = chartPayoutsDate;
            chartPayouts["config"]["options"]["title"]["text"] = "Current pool payouts " + localStorage.getItem("current-coin").toUpperCase() + " (+00:00 UTC)";
            window.chartPayouts.update();
          };
          $('#chart-poolpayouts').css('filter','unset');
          $('#chart-poolpayouts').closest('div').children('.error').not('.uk-hidden').addClass('uk-hidden');
        } else {
          chartPayoutsDate = {
            labels: ['0','1','2','0','1','2'],
            datasets: [{
                label: 'Coins',
                backgroundColor: 'rgb(54, 162, 235)',
                data: ['0','1','2','0','1','2']
            }]
          };
          if (startChartPayouts){
            startChartPayouts = false;
            runChartPayouts();
          };
          $('#chart-poolpayouts').css({'filter':'blur(2px)','-webkit-filter':'blur(5px)','-moz-filter':'blur(5px)','-ms-filter':'blur(5px)'});
          $('#chart-poolpayouts').closest('div').children('.error').removeClass('uk-hidden');
        }
      }
  });
};

function runChartPayoutsDonut(){
  window.chartPayoutsDonut = new Chart(document.getElementById("chart-donut").getContext("2d"), {
    type: 'doughnut',
    data: {
      datasets: [{
        data: chartPayoutsDate,
        backgroundColor: [
          'rgb(247, 147, 26)',
          'rgb(28, 117, 188)',
          'rgb(140, 140, 140)'
        ],
      }],
      labels: [ 'BTC', 'DASH', 'LTC' ]
    },
    options: {
      responsive: false,
      maintainAspectRatio: true,
      title: {
        display: false,
      },
      animation: {
        animateScale: true,
        animateRotate: true
      }
    }
  });
};
function runChartPoolrate(){
  window.chartPoolrate = new Chart(document.getElementById("chart-poolrate").getContext("2d"), {
    type: 'bar',
    data: chartPoolrateDate,
    ticks: {
      min: -3,
      max: 3,
      stepSize: 3,
      reverse: true,
    },
    options: {
      title:{ display:true, fontSize: 15, fontFamily: "Raleway", text:"Pool rate " + localStorage.getItem("current-coin").toUpperCase() + " " + currentHash + " (+00:00 UTC)" },
      tooltips: { mode: 'index', intersect: false },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          stacked: true,
          ticks: { maxTicksLimit: 7,maxRotation: chartRotation },
        }],
        yAxes: [{ stacked: true, beginAtZero:true }]
      }
    }
  });
};
function runChartPayouts(){
  window.chartPayouts = new Chart(document.getElementById("chart-poolpayouts").getContext("2d"), {
    type: 'bar',
    data: chartPayoutsDate,
    ticks: {
      min: -3,
      max: 3,
      stepSize: 3,
      reverse: true,
    },
    options: {
      title:{ display:true, fontSize: 15, fontFamily: "Raleway", text:"Current pool payouts " + localStorage.getItem("current-coin").toUpperCase() + " (+00:00 UTC)" },
      tooltips: { mode: 'index', intersect: false },
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          stacked: true,
          ticks: { maxTicksLimit: 7,maxRotation: chartRotation },
        }],
        yAxes: [{ stacked: true, beginAtZero:true }]
      }
    }
  });
};

$(document).ready(function(){
  $(".choose-coin a").click(function(e){
    if (!$(this).hasClass('disabled')){
      e.preventDefault();
      setDataChart();
      $(".choose-coin li").removeClass("uk-active");
      $(this).parent().addClass("uk-active");
      $('#id_coin').val(localStorage.getItem("current-coin"));
      $('#id_wallet').val();
    }
  });
  $(".choose-time a").click(function(e){
    e.preventDefault();
    if (!$(this).hasClass('disabled')){
      runBockerButton($(this));
      timePeriod=$(this).data('lang');
      setDataChart();
      $(".choose-time a").removeClass("uk-active");
      $(this).addClass("uk-active");
    } else {
      showForbiddenMessage();
    }
  });
  $("form button[type=reset]").click(function(e){
    e.preventDefault();
    $(this).closest('form').find('input[type=text]').val('');
  });

  setTimeout(function(){
    $('.chartjs-wrapper .uk-spinner').fadeOut();
    setDataChart();
  }, 600);
  setTimeout(function(){
    $('.chartjs-wrapper .uk-background-white').fadeOut();
  }, 1200);
  var currentCoin = localStorage.getItem('current-coin');
  $(".choose-coin").find('a.' + currentCoin).closest("li").addClass("uk-active");
  $('#id_coin').val(localStorage.getItem("current-coin"));
});

$(window).on("resize", function (){
  if(window.innerWidth>959){
    heightHeader = document.querySelectorAll('.main-container>header')[0].offsetHeight;
    heightFooter = document.querySelectorAll('.main-container>footer')[0].offsetHeight;
    heightFinal = (window.innerHeight - heightHeader - heightFooter) / 2 - 60;
    $('.main-container').find('.chartjs-wrapper:not(.uk-height-1-1)').css('height', heightFinal);
  } else {
    $('.main-container').find('.chartjs-wrapper:not(.uk-height-1-1)').css('height','200px');
  };
});
