from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from registration.forms import SignupForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from wallet.models import Wallet

# from email.mime.image import MIMEImage



@login_required
def home(request):
    return render(request, 'home/index.html')


def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        # if form.is_valid()&&(User.objects.filter(email=form.email)==None)&&(User.objects.filter(username=form.username)==None):
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject, from_email = 'Activate your account.', 'noreply@owlpull.com'
            html_content = render_to_string('registration/activate.html',{
                'user': user,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token':account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            text_content = strip_tags(html_content)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            message_text = 'Now you will recieve an email with instructions to how activate yours account'
            return render(request,'home/info.html', {'message':message_text,'to_email':to_email })
    else:
        form = SignupForm()
    return render(request, 'registration/signup.html', {'form': form})

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        Wallet.objects.create(user = user,is_active = True)
        e = Wallet.objects.get(user__username=user)
        e.get_stats()
        e.save()
        # print(e.tmp_value)
        to_email = user.email
        login(request, user)
        message = 'Thank you for your email confirmation. Now you are ready to mine.'
        return render(request,'home/info.html', {'message':message, 'to_email':to_email})
    else:
        message = 'Activation link is invalid!, please try signup again!'
        return render(request,'home/info.html', {'message':message})
