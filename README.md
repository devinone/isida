# Dashboard for interactive monitoring of coin mining nodes

## The goal of this project was to make a clear dashboard for monitoring the operation of equipment and automatic calculation of mining rewards.

[![screen.jpg](https://i.postimg.cc/N0QpZbjh/screen.jpg)](https://postimg.cc/PCFWwWDK)