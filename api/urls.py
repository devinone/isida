from django.conf.urls import url, include
# from django.utils.translation import ugettext as _
from api import views


urlpatterns = [
    # api home page
    url(r'^event/(?P<server>[a-z0-9]{1,8})/pool/(?P<coin>[a-z]{1,4})/(?P<time_period>[a-z]{1,6})/$', views.json_home_graph_power, name='json_home_graph_power'),
    url(r'^event/(?P<server>[a-z0-9]{1,8})/payouts/(?P<coin>[a-z]{1,4})/(?P<time_period>[a-z]{1,6})/$', views.json_home_graph_payouts, name='json_home_graph_payouts'),
    url(r'^event/(?P<server>[a-z0-9]{1,8})/info/(?P<coin>[a-z]{1,4})/$', views.json_home_table_pool_info, name='json_home_table_pool_info'),
    url(r'^event/(?P<server>[a-z0-9]{1,8})/info/private/$', views.json_home_table_private_info, name='json_home_table_private_info'),# check len of btc address and also for other coins
    url(r'^event/(?P<server>[a-z0-9]{1,8})/blocks/(?P<coin>[a-z]{1,4})/$', views.json_founded_blocks, name='json_founded_blocks'),

    #api profile page
    # url(r'^event/profile/info/$', views.json_profile_table_private_info, name='json_profile_table_private_info'),
    url(r'^event/(?P<server>[a-z0-9]{1,8})/profile/graph/(?P<coin>[a-z]{1,4})/(?P<time_period>[a-z]{1,6})/$', views.json_private_graph_power, name='json_private_graph_power'),
    url(r'^event/(?P<server>[a-z0-9]{1,8})/profile/payouts/(?P<coin>[a-z]{1,4})/(?P<time_period>[a-z]{1,6})/$', views.json_private_graph_payouts, name='json_private_graph_payouts'),

    url(r'^event/(?P<server>[a-z0-9]{1,8})/profile/(?P<time_period>[a-z]{1,6})/$', views.json_private_rewards, name='json_private_rewards'),




    # Not using yet!
    url(r'^event/(?P<server>[a-z0-9]{1,8})/local/(?P<coin>[a-z]{1,4})/(?P<time_period>[a-z]{1,6})/$', views.json_local_graph_power, name='json_local_graph_power'),
]
