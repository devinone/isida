import time, json, requests
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.utils.translation import activate
from datetime import datetime, timedelta
from django.contrib.auth.models import User
from wallet.models import Wallet
from wallet.views import validate_wallets

# Create your views here.

def json_home_graph_payouts(request,server,coin,time_period):
    if request.is_ajax():
        if request.method == 'GET':
            if ((coin == 'dash')or(coin == 'btc')or(coin == 'lite'))and((time_period =='hour')or(time_period =='day')or(time_period =='week')or(time_period =='month')or(time_period =='year')):
                output = loadHomePagePayouts(server,coin,time_period)
                return JsonResponse(output)
            else:
                print ('Bad json-request')
                return HttpResponse("Bad Arguments")
        else:
            return HttpResponse("Bad request")
    else:
        return HttpResponse("Bad request")

def loadHomePagePayouts(server,coin_select,time_period_select):
    if server=='europe2':
        pool_ip='89.223.27.250'
    else:
        pool_ip='195.208.108.144'
    ###
    if (coin_select=='dash'):
            pool_coin_port='7903'
    elif (coin_select=='btc'):
            pool_coin_port='9332'
    elif (coin_select=='lite'):
            pool_coin_port='9327'
    data = []
    name_json='http://'+pool_ip+':'+pool_coin_port+'/web/graph_data/current_payouts/last_'+time_period_select
    try:
        out = requests.get(url=name_json)
        data.append(out.json())
        data_time=[]
        data_payouts=[]
        if (time_period_select=='hour')or(time_period_select=='day'):
            for i in reversed(data[0]):
                utc_time=time.gmtime(int(i[0]))
                data_time.append(time.strftime("%H:%M", utc_time))
                ### parse good hashrate in H/s etc
                if (i[1]==None)or(i[1]=="Null")or(i[1]==0)or(i[1]=="null")or(i[1]=={}):
                    data_payouts.append(0)
                else:
                    try:
                        data_payouts.append(round(sum(i[1].values()),3))
                    except:
                        data_payouts.append(0)
            return {"success":"true","data_time":data_time, "data_payouts":data_payouts}
        else:
            for i in reversed(data[0]):
                utc_time=time.gmtime(int(i[0]))
                data_time.append(time.strftime("%d.%m.%y", utc_time))
                ### parse good hashrate in H/s etc
                if (i[1]==None)or(i[1]=="Null")or(i[1]==0)or(i[1]=="null")or(i[1]=={}):
                    data_payouts.append(0)
                else:
                    try:
                        data_payouts.append(round(sum(i[1].values()),3))
                        # print(round(sum(i[1].values()),3))
                    except:
                        data_payouts.append(0)
            return {"success":"true","error": "false", "data_time":data_time, "data_payouts":data_payouts}
    except:
        return {"success":"false","error":"500"}
################################################################################

def json_home_table_private_info(request,server):
    if request.is_ajax():
        ajax_request = json.loads(request.body.decode('utf-8'))
        if request.method == 'POST':
            if ((ajax_request['coin'] == 'dash')or(ajax_request['coin'] == 'btc')or(ajax_request['coin'] == 'lite')):
                output_dict = loadHomePagePrivateInfo(server,ajax_request['coin'],ajax_request['wallet'])
                return JsonResponse(output_dict)
                # return JsonResponse({'output':output_dict})
                #the output json dict {private_hashrate,private_rewards24,private_rewards30d,private_rewards24_dollar,private_rewards30d_dollar,updated}
            else:
                # print (json.loads( request.body.decode('utf-8') ), 'if not working')
                return HttpResponse("Bad Arguments")
        else:
            return HttpResponse("Bad request not POST")
    else:
        return HttpResponse("Bad request")


def json_profile_table_private_info(request,server):
    if request.is_ajax():
        ajax_request = json.loads(request.body.decode('utf-8'))
        if request.method == 'POST':
            if (User.objects.filter(username=request.user).exists()):
                output_dict = loadProfilePagePrivateInfo(server,request.user)
                return JsonResponse(output_dict)
                # return JsonResponse({'output':output_dict})
                #the output json dict {private_hashrate,private_rewards24,private_rewards30d,private_rewards24_dollar,private_rewards30d_dollar,updated}
            else:
                return HttpResponse("Bad Arguments")
        else:
            return HttpResponse("Bad request not POST")
    else:
        return HttpResponse("Bad request")

def loadProfilePagePrivateInfo(server,user):
    e = Wallet.objects.get(user__username=user)
    if server=='europe2':
        pool_ip='89.223.27.250'
    else:
        pool_ip='195.208.108.144'
    ###
    btc_stat  = {"id":1,"name":"Bitcoin","tag":"BTC","algorithm":"SHA-256","block_time":"581.0","block_reward":12.7049,
    "block_reward24":12.6880720430107,"block_reward3":12.6689655527984,"block_reward7":12.721592723092,"last_block":514354,
    "difficulty":3462542391191.6,"difficulty24":3462542391191.6,"difficulty3":3414047508453.07,"difficulty7":3349494587992.38,
    "nethash":25596396439210946643,"exchange_rate":8472.3,"exchange_rate24":8433.85212765958,"exchange_rate3":8017.53363932739,
    "exchange_rate7":8152.86,"exchange_rate_vol":65732.47179523,"exchange_rate_curr":"BTC","market_cap":"$143,430,175,262","pool_fee":"0.000000",
    "estimated_rewards":"0.001033","btc_revenue":"0.00103338","revenue":"$8.76","cost":"$3.29","profit":"$5.47","status":"Active","lagging":false,"timestamp":1521530291}
    btc_list = {"name":'btc',
                "pool_coin_port":'9332',
                "pool_ip":pool_ip,
                "coin_market":'1',
                "scalar_power":pow(10,12),
                "default_whttomine":14,
                "btc_price":1,
                "dimension_hash_rate":"TH/s",
                "wallet":e.wallet_addres_btc}
    dash_list = {"name":'dash',
                "pool_coin_port":'7903',
                "pool_ip":pool_ip,
                "coin_market":'34',
                "scalar_power":pow(10,9),
                "default_whttomine":17.8,
                "btc_price":round(requests.get(url='https://whattomine.com/coins/1.json').json()['exchange_rate3'],2),
                "dimension_hash_rate":"GH/s",
                "wallet":e.wallet_addres_dash}
    lite_list = {"name":'lite',
                "pool_coin_port":'9327',
                "pool_ip":pool_ip,
                "coin_market":'4',
                "scalar_power":pow(10,6),
                "default_whttomine":500,
                "btc_price":round(requests.get(url='https://whattomine.com/coins/1.json').json()['exchange_rate3'],2),
                "dimension_hash_rate":"MH/s",
                "wallet":e.wallet_addres_lite}

    coin_list = [btc_list,dash_list,lite_list]
    ######################################
    output = [] ### this is final out

    check_is_here = 0
    # print(dash_list['wallet'])
    for coin in coin_list:
        data = []
        tmp = {
        "name":coin['name'],
        "success": "false",
        "private_hashrate":0,
        "private_dead_hashrate":0,
        "private_rewards24":0,
        "private_rewards30d":0,
        "private_rewards24_dollar":0,
        "private_rewards30d_dollar":0,
        "updated":"None",
        "dimension_hash_rate":coin['dimension_hash_rate'],
        "error":"None"
        }
        try:
            name_json='http://'+coin['pool_ip']+':'+coin['pool_coin_port']+'/local_stats'
            out = requests.get(url=name_json)
            data.append(out.json())
            tmp['private_hashrate'] = round((data[0]['miner_hash_rates'][coin['wallet']])/(coin['scalar_power']),2)# IN GH/S for dash or TH/S for btc
            tmp['private_dead_hashrate'] = round((data[0]['miner_dead_hash_rates'][coin['wallet']])/(coin['scalar_power']),2)# IN GH/S for dash or TH/S for btc
            check_is_here = 1
        except:
            tmp['error'] = "Didn't find stats with this wallet"
            output.append(tmp)
            continue

        if check_is_here==1:
            coin_info = []
            name_json='https://whattomine.com/coins/'+coin['coin_market']+'.json'
            try:
                out = requests.get(url=name_json)
                coin_info.append(out.json())
                price = coin_info[0]['exchange_rate3']
                estimated_rewards = coin_info[0]['estimated_rewards'] # per 24h with 13,67 TH/S with electricity in coins
                tmp['private_rewards24'] = round(float(estimated_rewards)*tmp['private_hashrate']/coin['default_whttomine'],6) # with electricity 24 hours in coins
                tmp['private_rewards30d'] = round(float(estimated_rewards)*tmp['private_hashrate']/coin['default_whttomine']*30,6) # with electricity 30 days in coins

                tmp['private_rewards24_dollar'] = round(tmp['private_rewards24']*price*coin['btc_price'],2)# with electricity 24 hours
                tmp['private_rewards30d_dollar'] = round(tmp['private_rewards30d']*price*coin['btc_price'],2) # with electricity 30 days
                tmp['updated'] = time.strftime("%d %b %Y %H:%M", time.gmtime())
                tmp['success'] = "true"
                output.append(tmp)
                continue
            except:
                tmp['error'] = "Json error or during calculating stats"
                output.append(tmp)
                continue
        else:
            tmp['error'] = "Unexpected error"
            output.append(tmp)
            continue
    return output



def loadHomePagePrivateInfo(server, coin_select, wallet):
    coin_market=1
    if server=='europe2':
        pool_ip='89.223.27.250'
    else:
        pool_ip='195.208.108.144'
    ###
    if (coin_select=='dash'):
            pool_coin_port='7903'
            # pool_ip='195.208.108.144'
            #pool_ip='89.223.27.250'
            coin_market = "34"
            scalar_power = pow(10,9) # in Gh/s
            default_whttomine = 15; #this is default hashate for calculate profit in Gh/s
            btc_price = round(requests.get(url='https://whattomine.com/coins/1.json').json()['exchange_rate3'],2) #scalar for prfoit in $ - for btc of course is btc coins because its json stucture
            dimension_hash_rate = "GH/s"
    elif (coin_select=='btc'):
            pool_coin_port='9332'
            # pool_ip='195.208.108.144'
            coin_market = "1"
            scalar_power = pow(10,12)# in Th/s
            default_whttomine = 14; #this is default hashate for calculate profit in Th/s
            btc_price = 1 ; #scalar for prfoit in $ - for btc of course is 1
            dimension_hash_rate = "TH/s"
    elif (coin_select=='lite'):
            pool_coin_port='9327'
            # pool_ip='195.208.108.144'
            coin_market = "4"
            scalar_power = pow(10,6)
            default_whttomine = 500; #this is default hashate for calculate profit in Th/s
            btc_price = 1 ; #scalar for prfoit in $ - for btc of course is 1
            dimension_hash_rate = "MH/s"

    ######################################
    data = []
    check_is_here = 0
    try:
        name_json='http://'+pool_ip+':'+pool_coin_port+'/local_stats'
        out = requests.get(url=name_json)
        data.append(out.json())
        private_hashrate = round((data[0]['miner_hash_rates'][wallet])/(scalar_power),2)# IN GH/S for dash or TH/S for btc
        check_is_here = 1
    except:
        return {"success": "false", "private_hashrate":0,"private_rewards24":0,"private_rewards30d":0,
                    "private_rewards24_dollar":0,"private_rewards30d_dollar":0,"updated":"None","dimension_hash_rate":dimension_hash_rate}

    if check_is_here==1:

        # btc_price = round(requests.get(url='https://whattomine.com/coins/1.json').json()['exchange_rate3'],2)

        coin_info = []
        name_json='https://whattomine.com/coins/'+coin_market+'.json'
        try:
            out = requests.get(url=name_json)
            coin_info.append(out.json())
            price = coin_info[0]['exchange_rate3']
            estimated_rewards = coin_info[0]['estimated_rewards'] # per 24h with 13,67 TH/S with electricity in coins

            private_rewards24 = round(float(estimated_rewards)*private_hashrate/default_whttomine,6) # with electricity 24 hours in coins
            private_rewards30d = round(float(estimated_rewards)*private_hashrate/default_whttomine*30,6) # with electricity 30 days in coins

            private_rewards24_dollar = round(private_rewards24*price*btc_price,2)# with electricity 24 hours
            private_rewards30d_dollar = round(private_rewards30d*price*btc_price,2) # with electricity 30 days
            updated = time.strftime("%d %b %Y %H:%M", time.gmtime())

            # print(private_hashrate, private_rewards24, private_rewards30d, private_rewards24_dollar, private_rewards30d_dollar,updated )

            return {"success": "true","error": "false", "private_hashrate":private_hashrate,"private_rewards24":private_rewards24,"private_rewards30d":private_rewards30d,
                        "private_rewards24_dollar":private_rewards24_dollar,"private_rewards30d_dollar":private_rewards30d_dollar,"updated":updated,"dimension_hash_rate":dimension_hash_rate}
        except:
            return {"success":"false","error":"Oups! didn't find worker with wallet:"}

    else:
        return {"success": "true","error": "false", "private_hashrate":0,"private_rewards24":0,"private_rewards30d":0,
                    "private_rewards24_dollar":0,"private_rewards30d_dollar":0,"updated":"None","dimension_hash_rate":dimension_hash_rate}
################################################################################

def json_home_table_pool_info(request,server, coin):
    if request.is_ajax():
        if request.method == 'GET':
            if ((coin == 'dash')or(coin == 'btc')or(coin == 'lite')):
                output = loadHomePagePoolInfo(server,coin)
                return JsonResponse(output)
            else:
                print ('Bad json-request')
                return HttpResponse("Bad Arguments")
        else:
            return HttpResponse("Bad request")
    else:
        return HttpResponse("Bad request")

def loadHomePagePoolInfo(server, coin_select):
    if server=='europe2':
        pool_ip='89.223.27.250'
    else:
        pool_ip='195.208.108.144'
    ###
    if (coin_select=='dash'):
            pool_coin_port='7903'
            scalar_power = pow(10,9)
            dimension_hash_rate = "GH/s"
    elif (coin_select=='btc'):
            pool_coin_port='9332'
            scalar_power = pow(10,12)
            dimension_hash_rate = "TH/s"
    elif (coin_select=='lite'):
            pool_coin_port='9327'
            scalar_power = pow(10,6)
            dimension_hash_rate = "MH/s"
    data = []
    try:
        name_json='http://'+pool_ip+':'+pool_coin_port+'/local_stats'
        out = requests.get(url=name_json)
        data.append(out.json())

        uptime = data[0]['uptime']
        utc_time=time.gmtime(int(uptime))
        # uptime = time.strftime("%m m %d days %H hour %M minutes", utc_time)
        uptime = ("{0} month {1} days {2} hours {3} minutes".format(utc_time[1]-1, utc_time[2]-1, utc_time[3], utc_time[4])) # minus -1 for month day and year -1970 are needed
        pool_fee = data[0]['fee']

        try:
            pool_efficiency = round(data[0]['efficiency']*100,2)
        except:
            pool_efficiency = 0

        shares_total = data[0]['shares']['total']
        shares_orphan = data[0]['shares']['orphan']
        shares_dead = data[0]['shares']['dead']
        block_value = data[0]['block_value']
        try:
            effective_local_pool_hashrate = round(sum(data[0]['miner_hash_rates'].values())/scalar_power,2)# IN TH/S
        except:
            effective_local_pool_hashrate = 0

        try:
            dead_local_pool_hashrate = round(sum(data[0]['miner_dead_hash_rates'].values())/scalar_power,2)# IN TH/S
        except:
            dead_local_pool_hashrate = 0

        try:
            DOA_percent = round(dead_local_pool_hashrate/effective_local_pool_hashrate,2)
        except:
            DOA_percent = 0

        try:
            number_of_workers = len(data[0]['miner_hash_rates'])
        except:
            number_of_workers = 0

        try:
            current_ave_difficulty = int(sum(data[0]['miner_last_difficulties'].values())/len(data[0]['miner_last_difficulties']))
        except:
            current_ave_difficulty = "infinity"
        ###tmp calc
        if effective_local_pool_hashrate==0:
            time_to_share = "infinity"
            time_to_block = "infinity"
        else:
            ###Download node hashrate in H/s from json
            data_tmp = []
            name_json='http://'+pool_ip+':'+pool_coin_port+'/global_stats'
            out = requests.get(url=name_json)
            data_tmp.append(out.json())
            tmp_hash_rate_pool = (round(data_tmp[0]['pool_hash_rate'])) #IN H/S
            ###end block

            tmp_hash_rate_local = (round(sum(data[0]['miner_hash_rates'].values()))) #Local Hashrate IN H/S

            tmp_attempts_to_share = ((data[0]['attempts_to_share'])/tmp_hash_rate_local) # seconds
            tmp_attempts_to_block = ((data[0]['attempts_to_block'])/tmp_hash_rate_pool) # seconds

        ###

            # time_to_share = time.strftime("%d d %H h %M m", time.gmtime(tmp_attempts_to_share))
            # time_to_block = time.strftime("%d d %H h %M m", time.gmtime(tmp_attempts_to_block))
            time_to_share = str(timedelta(seconds=int(tmp_attempts_to_share)))
            time_to_block = str(timedelta(seconds=int(tmp_attempts_to_block)))
            # print(sec1)
            # print(sec2)



        #print(current_ave_difficulty)
        output = {"success": "true", "error": "false", "effective_local_pool_hashrate":effective_local_pool_hashrate, "dead_local_pool_hashrate":dead_local_pool_hashrate, "pool_efficiency":pool_efficiency,
            "pool_fee":pool_fee, "number_of_workers":number_of_workers, "shares_total":shares_total,  "shares_orphan":shares_orphan, "shares_dead":shares_dead, "time_to_share":time_to_share,
            "time_to_block":time_to_block, "current_ave_difficulty":current_ave_difficulty, "uptime":uptime,"dimension_hash_rate":dimension_hash_rate }


        return output #hashrate good, hashrate bad, efficiency, fee, number workers, shares g/o/d, time to share, time to block, curr dif, uptime

    except:
        # return {"success":"false","error":"500","dimension_hash_rate":dimension_hash_rate} # this is table pool info
        return {"success":"false","error":"500","effective_local_pool_hashrate":0, "dead_local_pool_hashrate":0, "pool_efficiency":0,
            "pool_fee":0, "number_of_workers":0, "shares_total":0,  "shares_orphan":0, "shares_dead":0, "time_to_share":'infinity',
            "time_to_block":'infinity', "current_ave_difficulty":0, "uptime":"0 days","dimension_hash_rate":"Gh/s"} # this is table pool info


################################################################################

def json_home_graph_power(request,server,coin,time_period):
    if request.is_ajax():
        if request.method == 'GET':
            if ((coin == 'dash')or(coin == 'btc')or(coin == 'lite'))and((time_period =='hour')or(time_period =='day')or(time_period =='week')or(time_period =='month')or(time_period =='year')):
                output = loadHomePagePower(server,coin,time_period)
                return JsonResponse(output)
                # return JsonResponse({'data_time':data_time,'data_power_good':data_power_good,'data_power_orphan':data_power_orphan,'data_power_doa':data_power_doa})
            else:
                print ('Bad json-request')
                return HttpResponse("Bad Arguments")
        else:
            return HttpResponse("Bad request")
    else:
        return HttpResponse("Bad request")


    # return HttpResponse("OK")

def loadHomePagePower(server,coin_select,time_period_select):
    if server=='europe2':
        pool_ip='89.223.27.250'
    else:
        pool_ip='195.208.108.144'
    ###
    if (coin_select=='dash'):
            pool_coin_port='7903'
            # pool_ip='195.208.108.144'
            scalar_power = pow(10,9)
            #pool_ip='89.223.27.250'
            dimension_hash_rate = "GH/s"
    elif (coin_select=='btc'):
            # pool_ip='195.208.108.144'
            pool_coin_port='9332'
            scalar_power = pow(10,12)
            dimension_hash_rate = "TH/s"
    elif (coin_select=='lite'):
            # pool_ip='195.208.108.144'
            pool_coin_port='9327'
            scalar_power = pow(10,6)
            dimension_hash_rate = "MH/s"


    data = []
    name_json='http://'+pool_ip+':'+pool_coin_port+'/web/graph_data/pool_rates/last_'+time_period_select
    try:
        out = requests.get(url=name_json)
        data.append(out.json())
        data_time=[]
        data_power_good=[]
        data_power_orphan=[]
        data_power_doa=[]
        if (time_period_select=='hour')or(time_period_select=='day'):
            for i in reversed(data[0]):
                utc_time=time.gmtime(int(i[0]))
                data_time.append(time.strftime("%H:%M", utc_time))
            #     ### parse good hashrate in H/s etc
                if (i[1]==None)or(i[1]=="Null")or(i[1]==0)or(i[1]=="null"):
                    data_power_good.append(0)
                    data_power_orphan.append(0)
                    data_power_doa.append(0)
                else:
                    data_power_good.append(round(float(i[1]['good'])/(scalar_power),2))
                    data_power_orphan.append(round(float(i[1]['orphan'])/(scalar_power),2))
                    try:
                        data_power_doa.append(round(float(i[1]['doa'])/(scalar_power),2))
                    except:
                        data_power_doa.append(0)
            return {"success": "true", "data_time":data_time, "data_power_good":data_power_good, "data_power_orphan":data_power_orphan,"data_power_doa":data_power_doa,"dimension_hash_rate":dimension_hash_rate}
        else:
            for i in reversed(data[0]):
                utc_time=time.gmtime(int(i[0]))
                data_time.append(time.strftime("%d.%m.%y", utc_time))
            #     ### parse good hashrate in H/s etc
                if (i[1]==None)or(i[1]=="Null")or(i[1]==0)or(i[1]=="null"):
                    data_power_good.append(0)
                    data_power_orphan.append(0)
                    data_power_doa.append(0)
                else:
                    data_power_good.append(round(float(i[1]['good'])/(scalar_power),2))
                    data_power_orphan.append(round(float(i[1]['orphan'])/(scalar_power),2))
                    try:
                        data_power_doa.append(round(float(i[1]['doa'])/(scalar_power),2))
                    except:
                        data_power_doa.append(0)
            return {"success": "true", "error": "false", "data_time":data_time, "data_power_good":data_power_good, "data_power_orphan":data_power_orphan,"data_power_doa":data_power_doa,"dimension_hash_rate":dimension_hash_rate}
    except:
        return {"success":"false","error":"500","dimension_hash_rate":dimension_hash_rate}

################################################################################

def json_private_graph_power(request,server,coin, time_period):
    if User.objects.filter(username=request.user).exists():
        e = Wallet.objects.get(user__username=request.user)
        if request.is_ajax():
            if request.method == 'GET':
                if ((coin == 'dash')or(coin == 'btc')or(coin == 'lite'))and((time_period =='hour')or(time_period =='day')or(time_period =='week')or(time_period =='month')or(time_period =='year')):
                    if (coin == 'btc'):
                        wallet = e.wallet_addres_btc
                    elif (coin == 'dash'):
                        wallet = e.wallet_addres_dash
                    else:
                        wallet = e.wallet_addres_lite
                    output_total = loadPrivatePagePower_TOTAL(server,coin,time_period,wallet)
                    output_dead = loadPrivatePagePower_DEAD(server,coin,time_period,wallet)
                    # output_payouts = loadPrivatePagePower_PAYOUTS(coin,time_period,wallet)
                    # print(output_total)
                    if (output_total["success"]=="true")and(output_dead["success"]=="true"):
                        return JsonResponse({"success": "true", "error":"false", "output_total":output_total,"output_dead":output_dead})

                    elif (output_total["success"]=="false")and(output_dead["success"]=="false"):
                        return JsonResponse({"success": "false","error":"Can't get all private stats", "output_total":output_total,"output_dead":output_dead})

                    else:
                        return JsonResponse({"success": "true", "error":"Can't get some private stats", "output_total":output_total,"output_dead":output_dead})
                else:
                    print ('Bad json-request')
                    return HttpResponse("Bad Arguments")
            else:
                return HttpResponse("Bad request")
        else:
            return HttpResponse("Bad request")
    else:
        return HttpResponse("Bad user request")

def json_private_graph_payouts(request,server,coin, time_period):
    if User.objects.filter(username=request.user).exists():
        e = Wallet.objects.get(user__username=request.user)

        if request.is_ajax():
            if request.method == 'GET':
                if ((coin == 'dash')or(coin == 'btc')or(coin == 'lite'))and((time_period =='hour')or(time_period =='day')or(time_period =='week')or(time_period =='month')or(time_period =='year')):
                    if (coin == 'btc'):
                        wallet = e.wallet_addres_btc
                    elif (coin == 'dash'):
                        wallet = e.wallet_addres_dash
                    else:
                        wallet = e.wallet_addres_lite
                    output_total = loadPrivatePagePower_TOTAL(server,coin,time_period,wallet)
                    # output_dead = loadPrivatePagePower_DEAD(coin,time_period,wallet)
                    output_payouts = loadPrivatePagePower_PAYOUTS(server,coin,time_period,wallet)
                    if (output_total["success"]=="true")and(output_payouts["success"]=="true"):
                        return JsonResponse({"success": "true", "error":"false", "data_time":output_total['data_time'],"output_payouts":output_payouts})

                    elif (output_total["success"]=="false")and(output_payouts["success"]=="false"):
                        return JsonResponse({"success": "false","error":"Can't get all private stats", "data_time":output_total['data_time'],"output_payouts":output_payouts})

                    else:
                        return JsonResponse({"success": "true", "error":"Can't get some private stats", "data_time":output_total['data_time'],"output_payouts":output_payouts})
                else:
                    print ('Bad json-request')
                    return HttpResponse("Bad Arguments")
            else:
                return HttpResponse("Bad request")
        else:
            return HttpResponse("Bad request")
    else:
        return HttpResponse("Bad user request")


def loadPrivatePagePower_TOTAL(server,coin_select,time_period_select,wallet):
    if server=='europe2':
        pool_ip='89.223.27.250'
    else:
        pool_ip='195.208.108.144'
    ###
    if (coin_select=='dash'):
            pool_coin_port='7903'
            # pool_ip='195.208.108.144'
            scalar_power = pow(10,9)
            #pool_ip='89.223.27.250'
            dimension_hash_rate = "GH/s"
    elif (coin_select=='btc'):
            # pool_ip='195.208.108.144'
            pool_coin_port='9332'
            scalar_power = pow(10,12)
            dimension_hash_rate = "TH/s"
    elif (coin_select=='lite'):
            # pool_ip='195.208.108.144'
            pool_coin_port='9327'
            scalar_power = pow(10,6)
            dimension_hash_rate = "MH/s"


    data = []
    name_json_total='http://'+pool_ip+':'+pool_coin_port+'/web/graph_data/miner_hash_rates/last_'+time_period_select
    try:
        out = requests.get(url=name_json_total)
        data.append(out.json())
        ###
        data_time=[]
        data_power_total=[]
        ### first load total_hashrate
        if (time_period_select=='hour')or(time_period_select=='day'):
            for i in reversed(data[0]):
                utc_time=time.gmtime(int(i[0]))
                data_time.append(time.strftime("%H:%M", utc_time))
                try:
                    if (i[1][wallet]==None)or(i[1][wallet]=="Null")or(i[1][wallet]==0)or(i[1][wallet]=="null")or(i[1]=='{}'):
                        data_power_total.append(0)
                    else:
                        try:
                            data_power_total.append(round(float(i[1][wallet])/(scalar_power),2))
                        except:
                            data_power_total.append(0)
                except:
                    data_power_total.append(0)
            return {"success": "true", "data_time":data_time, "data_power_total":data_power_total,"dimension_hash_rate":dimension_hash_rate}
        else:
            for i in reversed(data[0]):
                utc_time=time.gmtime(int(i[0]))
                data_time.append(time.strftime("%d.%m.%y", utc_time))
                try:
                    if (i[1][wallet]==None)or(i[1][wallet]=="Null")or(i[1][wallet]==0)or(i[1][wallet]=="null")or(i[1]=='{}'):
                        data_power_total.append(0)
                    else:
                        try:
                            data_power_total.append(round(float(i[1][wallet])/(scalar_power),2))
                        except:
                            data_power_total.append(0)
                except:
                    data_power_total.append(0)
            # print(data_power_total)
            return {"success": "true", "error": "false", "data_time":data_time, "data_power_total":data_power_total, "dimension_hash_rate":dimension_hash_rate}
    except:
        # print(data_power_total)
        return {"success":"false","error":"500","dimension_hash_rate":dimension_hash_rate}

def loadPrivatePagePower_DEAD(server,coin_select,time_period_select,wallet):
    if server=='europe2':
        pool_ip='89.223.27.250'
    else:
        pool_ip='195.208.108.144'
    ###
    if (coin_select=='dash'):
            pool_coin_port='7903'
            # pool_ip='195.208.108.144'
            scalar_power = pow(10,9)
            #pool_ip='89.223.27.250'
            dimension_hash_rate = "GH/s"
    elif (coin_select=='btc'):
            # pool_ip='195.208.108.144'
            pool_coin_port='9332'
            scalar_power = pow(10,12)
            dimension_hash_rate = "TH/s"
    elif (coin_select=='lite'):
            # pool_ip='195.208.108.144'
            pool_coin_port='9327'
            scalar_power = pow(10,6)
            dimension_hash_rate = "MH/s"


    data = []
    name_json_dead='http://'+pool_ip+':'+pool_coin_port+'/web/graph_data/miner_dead_hash_rates/last_'+time_period_select
    try:
        out = requests.get(url=name_json_dead)
        data.append(out.json())

        data_power_dead=[]
        ### first load total_hashrate
        if (time_period_select=='hour')or(time_period_select=='day'):
            for i in reversed(data[0]):
                try:
                    if (i[1][wallet]==None)or(i[1][wallet]=="Null")or(i[1][wallet]==0)or(i[1][wallet]=="null")or(i[1]=='{}'):
                        data_power_dead.append(0)
                    else:
                        try:
                            data_power_dead.append(round(float(i[1][wallet])/(scalar_power),2))
                        except:
                            data_power_dead.append(0)
                except:
                    data_power_dead.append(0)

            return {"success": "true", "error": "false", "data_power_dead":data_power_dead}
        else:
            for i in reversed(data[0]):
                try:
                    if (i[1][wallet]==None)or(i[1][wallet]=="Null")or(i[1][wallet]==0)or(i[1][wallet]=="null")or(i[1]=='{}'):
                        data_power_dead.append(0)
                    else:
                        try:
                            data_power_dead.append(round(float(i[1][wallet])/(scalar_power),2))
                        except:
                            data_power_dead.append(0)
                except:
                    data_power_dead.append(0)
            return {"success": "true", "error": "false", "data_power_dead":data_power_dead}
    except:
        return {"success":"false","error":"500"}

def loadPrivatePagePower_PAYOUTS(server,coin_select,time_period_select,wallet):
    if server=='europe2':
        pool_ip='89.223.27.250'
    else:
        pool_ip='195.208.108.144'
    ###
    if (coin_select=='dash'):
            pool_coin_port='7903'
            # pool_ip='195.208.108.144'
            scalar_power = pow(10,9)
            #pool_ip='89.223.27.250'
            dimension_hash_rate = "GH/s"
    elif (coin_select=='btc'):
            # pool_ip='195.208.108.144'
            pool_coin_port='9332'
            scalar_power = pow(10,12)
            dimension_hash_rate = "TH/s"
    elif (coin_select=='lite'):
            # pool_ip='195.208.108.144'
            pool_coin_port='9327'
            scalar_power = pow(10,6)
            dimension_hash_rate = "MH/s"


    data = []
    name_json_payouts='http://'+pool_ip+':'+pool_coin_port+'/web/graph_data/current_payouts/last_'+time_period_select
    try:
        out = requests.get(url=name_json_payouts)
        data.append(out.json())
        ###
        data_power_payouts=[]
        ### first load total_hashrate
        if (time_period_select=='hour')or(time_period_select=='day'):
            for i in reversed(data[0]):
                try:
                    if (i[1][wallet]==None)or(i[1][wallet]=="Null")or(i[1][wallet]==0)or(i[1][wallet]=="null")or(i[1]=='{}'):
                        data_power_payouts.append(0)
                    else:
                        try:
                            data_power_payouts.append(round(i[1][wallet],4))
                        except:
                            data_power_payouts.append(0)
                except:
                    data_power_payouts.append(0)
            return {"success": "true", "error": "false", "data_power_payouts":data_power_payouts}
        else:
            for i in reversed(data[0]):
                try:
                    if (i[1][wallet]==None)or(i[1][wallet]=="Null")or(i[1][wallet]==0)or(i[1][wallet]=="null")or(i[1]=='{}'):
                        data_power_payouts.append(0)
                    else:
                        try:
                            data_power_payouts.append(round(i[1][wallet],4))
                        except:
                            data_power_payouts.append(0)
                except:
                    data_power_payouts.append(0)
            return {"success": "true", "error": "false", "data_power_payouts":data_power_payouts}
    except:
        return {"success":"false","error":"500"}


################################################################################

def json_private_rewards(request,server, time_period):
    if User.objects.filter(username=request.user).exists():
        e = Wallet.objects.get(user__username=request.user)
        if request.is_ajax():
            if request.method == 'GET':
                checked = validate_wallets(e.wallet_addres_btc, e.wallet_addres_dash, e.wallet_addres_lite)
                if ((checked['btc']=="")and(checked['dash']=="")and(checked['ltc']==""))and((time_period =='hour')or(time_period =='day')or(time_period =='week')or(time_period =='month')or(time_period =='year')):
                    btc_price, output_rewards = loadPrivatePageRewards(server,time_period,e.wallet_addres_btc, e.wallet_addres_dash, e.wallet_addres_lite)
                    if (output_rewards[0]["success"]=="true")and(output_rewards[1]["success"]=="true")and(output_rewards[2]["success"]=="true"):
                        return JsonResponse({"success": "true", "error":"false", "output_rewards":output_rewards,"btc_price":btc_price})
                    elif (output_rewards[0]["success"]=="false")and(output_rewards[1]["success"]=="false")and(output_rewards[2]["success"]=="false"):
                        return JsonResponse({"success": "false", "error":"Can't get all private stats, maybe all pool walltes setup?", "output_rewards":output_rewards,"btc_price":btc_price})
                    else:
                        return JsonResponse({"success": "true","error":"Can't get private some of stats","output_rewards":output_rewards,"btc_price":btc_price})
                else:
                    print ('Bad json-request')
                    return HttpResponse("Bad Arguments")
            else:
                return HttpResponse("Bad request")
        else:
            return HttpResponse("Bad request")
    else:
        return HttpResponse("Bad user request")


def loadPrivatePageRewards(server,time_period,btc_wallet, dash_wallet, litecoin_wallet):
    if server=='europe2':
        pool_ip='89.223.27.250'
    else:
        pool_ip='195.208.108.144'
    ###
    days = 7
    if time_period=="hour":
        days = round(1/24,2)
    elif time_period=="day":
        days = 1
    elif time_period=="week":
        days = 7
    elif time_period=="month":
        days = 30
    elif time_period=="year":
        days = 365
    btc_price = round(requests.get(url='https://whattomine.com/coins/1.json').json()['exchange_rate'],2)
    btc_list = {"name":'btc',
                "pool_coin_port":'9332',
                "pool_ip":pool_ip,
                "coin_market":'1',
                "scalar_power":pow(10,12),
                "default_whttomine":14,
                "btc_price":1,
                "dimension_hash_rate":"TH/s",
                "electricity":1370,### this is for us
                "wallet":btc_wallet}
    dash_list = {"name":'dash',
                "pool_coin_port":'7903',
                "pool_ip":pool_ip,
                "coin_market":'34',
                "scalar_power":pow(10,9),
                "default_whttomine":15,
                "btc_price":btc_price,
                "dimension_hash_rate":"GH/s",
                "electricity":1200,### this is for us
                "wallet":dash_wallet}
    lite_list = {"name":'lite',
                "pool_coin_port":'9327',
                "pool_ip":pool_ip,
                "coin_market":'4',
                "scalar_power":pow(10,6),
                "default_whttomine":500,
                "btc_price":btc_price,
                "dimension_hash_rate":"MH/s",
                "electricity":800,### this is for us
                "wallet":litecoin_wallet}

    coin_list = [btc_list,dash_list,lite_list]
    ######################################
    output = [] ### this is final out
    check_is_here = 0
    for coin in coin_list:
        data = []
        tmp = {
        "name":coin['name'],
        "success": "false",
        "private_hashrate":0,
        "private_rewards":0,
        "private_rewards_dollar":0,
        "btc_price":0,
        "updated":"None",
        "error":"None",
        }

        if coin['wallet'] =="":
            tmp['error'] = "Didn't find stats with this wallet"
            output.append(tmp)
            continue
        else:
            try:
                name_json='http://'+coin['pool_ip']+':'+coin['pool_coin_port']+'/local_stats'
                out = requests.get(url=name_json)
                data.append(out.json())
                tmp['private_hashrate'] = round((data[0]['miner_hash_rates'][coin['wallet']])/(coin['scalar_power']),2)# IN GH/S for dash or TH/S for btc
                check_is_here = 1
            except:
                tmp['error'] = "Didn't find stats with this wallet"
                output.append(tmp)
                continue

        if check_is_here==1:
            coin_info = []
            name_json='https://whattomine.com/coins/'+coin['coin_market']+'.json'
            try:
                out = requests.get(url=name_json)
                coin_info.append(out.json())
                price = coin_info[0]['exchange_rate3']
                estimated_rewards = coin_info[0]['estimated_rewards']
                tmp['private_rewards'] = round(float(estimated_rewards)*tmp['private_hashrate']/coin['default_whttomine']*days,6) # with electricity 7 days in coins
                tmp['private_rewards_dollar'] = round(tmp['private_rewards']*price*coin['btc_price'],2) # with electricity 7 days
                tmp['updated'] = time.strftime("%d %b %Y %H:%M", time.gmtime())
                tmp['success'] = "true"
                tmp['btc_price'] = coin['btc_price']
                output.append(tmp)
                continue
            except:
                tmp['error'] = "Json error or during calculating stats"
                output.append(tmp)
                continue
        else:
            tmp['error'] = "Unexpected error"
            output.append(tmp)
            continue
    return btc_price, output






# Not use yet!
################################################################################
def json_local_graph_power(request,server,coin,time_period):
    if request.is_ajax():
        if request.method == 'GET':
            if ((coin == 'dash')or(coin == 'btc')or(coin == 'lite'))and((time_period =='hour')or(time_period =='day')or(time_period =='week')or(time_period =='month')or(time_period =='year')):
                # data_time, data_power_good, data_power_dead = loadLocalPoolCoinPageStat(coin,time_period)
                # return JsonResponse({'data_time':data_time,'data_power_good':data_power_good,'data_power_dead':data_power_dead})
                output = loadLocalPower(server,coin,time_period)
                return JsonResponse(output)

            else:
                print ('Bad json-request')
                return HttpResponse("Bad Arguments")
        else:
            return HttpResponse("Bad request")
    else:
        return HttpResponse("Bad request")

def loadLocalPower(server,coin_select,time_period_select):
    if server=='europe2':
        pool_ip='89.223.27.250'
    else:
        pool_ip='195.208.108.144'
    ###
    if (coin_select=='dash'):
            pool_coin_port='7903'
            # pool_ip='195.208.108.144'
            #pool_ip='89.223.27.250'
            scalar_power = pow(10,9)
            dimension_hash_rate = "GH/s"
    elif (coin_select=='btc'):
            pool_coin_port='9332'
            # pool_ip='195.208.108.144'
            scalar_power = pow(10,12)
            dimension_hash_rate = "TH/s"
    elif (coin_select=='lite'):
            pool_coin_port='9327'
            # pool_ip='195.208.108.144'
            scalar_power = pow(10,6)
            dimension_hash_rate = "MH/s"
    data = []
    name_json='http://'+pool_ip+':'+pool_coin_port+'/web/graph_data/local_hash_rate/last_'+time_period_select
    try:
        out = requests.get(url=name_json)
        data.append(out.json())
        data_time=[]
        data_power_good=[]
        data_power_dead=[]
        if (time_period_select=='hour')or(time_period_select=='day'):
            for i in reversed(data[0]):
                utc_time=time.gmtime(int(i[0]))
                data_time.append(time.strftime("%H:%M", utc_time))
            #     ### parse good hashrate in H/s etc
                if (i[1]==None)or(i[1]=="Null")or(i[1]==0)or(i[1]=="null"):
                    data_power_good.append(0)
                    data_power_dead.append(0)
                else:
                    data_power_good.append(round(float(i[1])/(scalar_power),2))
                    data_power_dead.append(round(float(i[2])/(scalar_power),2))
            return {"success": "true", "data_time":data_time, "data_power_good":data_power_good, "data_power_dead":data_power_dead,"dimension_hash_rate":dimension_hash_rate}
        else:
            for i in reversed(data[0]):
                utc_time=time.gmtime(int(i[0]))
                data_time.append(time.strftime("%Y-%m-%d", utc_time))
            #     ### parse good hashrate in H/s etc
                if (i[1]==None)or(i[1]=="Null")or(i[1]==0)or(i[1]=="null"):
                    data_power_good.append(0)
                    data_power_dead.append(0)
                else:
                    data_power_good.append(round(float(i[1])/(scalar_power),2))
                    data_power_dead.append(round(float(i[2])/(scalar_power),2))
            return {"success": "true", "error": "false", "data_time":data_time, "data_power_good":data_power_good, "data_power_dead":data_power_dead,"dimension_hash_rate":dimension_hash_rate}
    except:
        return {"success":"false","error":"500","dimension_hash_rate":dimension_hash_rate}





#################additional functions

# def loadJSON_info_coin():
#
#     coins=['34','151','162'] #dash, etc,etc_c
#     data = []
#     for coin in coins:
#         name_json='https://whattomine.com/coins/'+coin+'.json'
#         out = requests.get(url=name_json)
#         data.append(out.json())
#     return data
