from django.conf.urls import url, include
from wallet import views


urlpatterns = [
    url(r'^profile/$', views.profile, name='profile'),
]
