from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from wallet.forms import WalletForm
from wallet.models import Wallet
from api import views as api
from coin.models import COIN

# Create your views here.


def profile(request):
    try:
        owl = COIN.objects.get(name="owl")
        owl.get_difftime()
        owl.update_coins()
        owl.save()
    except:
        owl = COIN.objects.create(name = "owl")
        owl.activate()
        owl.save()
    msg_log=""### indentify log message for form
    if request.method == 'POST':
        form = WalletForm(request.POST)
        if (form.is_valid())and(User.objects.filter(username=request.user).exists()):
            wallet_btc, wallet_dash, wallet_lite = form.get_save_wallets()
            msg_log = validate_wallets(wallet_btc, wallet_dash, wallet_lite)
            try:
                e = Wallet.objects.get(user__username=request.user)
            except:
                return render(request,'errors/500.html')
            if (msg_log['btc']==""):
                e.wallet_addres_btc = wallet_btc
            if (msg_log['dash']==""):
                e.wallet_addres_dash = wallet_dash
            if (msg_log['ltc']==""):
                e.wallet_addres_lite = wallet_lite
            e.save()
            form = WalletForm(instance=e)
            total = api.loadPrivatePagePower_TOTAL('europe1',"dash","hour",e.wallet_addres_dash)
            dead = api.loadPrivatePagePower_DEAD('europe1',"dash","hour",e.wallet_addres_dash)
            payouts = api.loadPrivatePagePower_PAYOUTS('europe1',"dash","hour",e.wallet_addres_dash)
            btc_price, rewards = api.loadPrivatePageRewards('europe1',"month", e.wallet_addres_btc, e.wallet_addres_dash, e.wallet_addres_lite)
            return render(request,'wallet/profile.html', { "form":form,"msg_log":msg_log,"total":total,"dead":dead,"payouts":payouts,"rewards":rewards,"btc_price":btc_price})
        else:
            msg_log = "Incorrect sended form"
            try:
                e = Wallet.objects.get(user__username=request.user)
                form = WalletForm(instance=e)
            except:
                form = WalletForm(instance=e)
            total = api.loadPrivatePagePower_TOTAL('europe1',"dash","hour",e.wallet_addres_dash)
            dead = api.loadPrivatePagePower_DEAD('europe1',"dash","hour",e.wallet_addres_dash)
            payouts = api.loadPrivatePagePower_PAYOUTS('europe1',"dash","hour",e.wallet_addres_dash)
            btc_price, rewards = api.loadPrivatePageRewards('europe1',"month", e.wallet_addres_btc, e.wallet_addres_dash, e.wallet_addres_lite)
            return render(request,'wallet/profile.html', { "form":form,"msg_log":msg_log,"total":total,"dead":dead,"payouts":payouts,"rewards":rewards,"btc_price":btc_price})

    else:
        if User.objects.filter(username=request.user).exists():
            try:
                e = Wallet.objects.get(user__username=request.user)
                form = WalletForm(instance=e)
            except:
                form = WalletForm()
            # output = api.loadProfilePagePrivateInfo(request.user)#coin_list = [btc_list,dash_list]
            total = api.loadPrivatePagePower_TOTAL('europe1',"dash","hour",e.wallet_addres_dash)
            dead = api.loadPrivatePagePower_DEAD('europe1',"dash","hour",e.wallet_addres_dash)
            payouts = api.loadPrivatePagePower_PAYOUTS('europe1',"dash","hour",e.wallet_addres_dash)
            btc_price, rewards = api.loadPrivatePageRewards('europe1',"month", e.wallet_addres_btc, e.wallet_addres_dash, e.wallet_addres_lite)

            # print(total)
            # print(dead)
            # print(payouts)
            # return render(request,'wallet/profile.html', { "form":form,"msg_log":msg_log,"btc_info":output[0],"dash_info":output[1],"ltc_info":output[2]})
            return render(request,'wallet/profile.html', { "form":form,"msg_log":msg_log,"total":total,"dead":dead,"payouts":payouts,"rewards":rewards,"btc_price":btc_price})
        else:
            return redirect('login')




def validate_wallets(wallet_btc, wallet_dash, wallet_ltc):
    output = {}
    try:
        if (((wallet_btc[0]=="1")or(wallet_btc[0]=="3"))and(27<=len(wallet_btc)<=34)):
            output['btc']=""
        else:
            output['btc']="BTC wallet isn't correct.Try to check BTC-wallet again."
    except:
        output['btc']=""

    try:
        if ((wallet_dash[0]=="X")and(27<=len(wallet_dash)<=34))or(wallet_dash==""):
            output['dash']=""
        else:
            output['dash']="Dash wallet isn't correct.Try to check Dash-wallet again."
    except:
        output['dash']=""


    try:
        if (((wallet_ltc[0]=="L")or(wallet_ltc[0]=="3"))and(27<=len(wallet_ltc)<=34))or(wallet_ltc==""):
            output['ltc']=""
        else:
            output['ltc']="Litecoin wallet isn't correct.Try to check Lite-wallet again."
    except:
        output['ltc']=""

    return output
    # if (((wallet_btc[0]=="1")or(wallet_btc[0]=="3"))and(27<=len(wallet_btc)<=34)):
    #     if ((wallet_dash[0]=="X")and(27<=len(wallet_dash)<=34)):
    #         if (((wallet_lite[0]=="L")or(wallet_lite[0]=="3"))and(27<=len(wallet_lite)<=34)):
    #             return ""
    #         else:
    #             return "Litecoin wallet isn't correct.Try to check Lite-wallet again."
    #     else:
    #         return "Dash wallet isn't correct.Try to check Dash-wallet again."
    # else:
    #     return "BTC wallet isn't correct.Try to check BTC-wallet again."
