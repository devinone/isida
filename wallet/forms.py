from django import forms
from wallet.models import Wallet



class WalletForm(forms.ModelForm):
    class Meta:
        model = Wallet
        fields = ('wallet_addres_btc','wallet_addres_dash','wallet_addres_lite')

    def get_save_wallets(self):
        wallet_btc = self.cleaned_data.get('wallet_addres_btc')
        wallet_dash = self.cleaned_data.get('wallet_addres_dash')
        wallet_lite = self.cleaned_data.get('wallet_addres_lite')
        return wallet_btc, wallet_dash, wallet_lite




#from wallet.choices import *

# class WalletEditingForm(forms.Form):
#     wallet_btc = forms.CharField(max_length = 45, required=True)
#     wallet_dash = forms.CharField(max_length = 45, required=True)
#
#



# coin = forms.ChoiceField(choices = COIN_CHOICES, label="", initial='', widget=forms.Select(), required=True)
# wallet = forms.CharField(max_length = 45, help_text = "Add your wallet here and press Save below...", required=True)
# class Meta:
#     model = Wallet
#     fields = ('coin','wallet_addres', )

# def clean_email(self):
#     email = self.cleaned_data.get('email')
#     username = self.cleaned_data.get('username')
#     if email and User.objects.filter(email=email).exclude(username=username).exists():
#         raise forms.ValidationError(u'Email addresses must be unique.')
#     return email
# def __str__(self):
#     return self.coin, self.wallet
