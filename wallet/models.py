from django.db import models
from django.contrib.auth.models import User
from wallet.choices import *
import json, requests
# from isida.settings import COIN1

class Wallet(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,verbose_name='User account')
    wallet_addres_btc = models.CharField(max_length = 45, default = "",blank=True,verbose_name='Address btc') # This is max length for all existing wallets
    wallet_addres_dash = models.CharField(max_length = 45, default = "",blank=True,verbose_name='Address dash') # This is max length for all existing wallets
    wallet_addres_lite = models.CharField(max_length = 45, default = "",blank=True,verbose_name='Address litecoin') # This is max length for all existing wallets
    is_active = models.BooleanField(default = False,verbose_name='Is active')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    tmp_value = models.FloatField(default = 0)


    class Meta:
        verbose_name="Personal wallet"
        verbose_name_plural="Personal wallets"

    def __str__(self):
                return '%s %s' % (self.user, self.is_active)

    def get_stats(self):
        # self.tmp_value = round(COIN1['exchange_rate3'],2)
        self.tmp_value = 8900
        self.save()

    # objects = User()
    #
    #
    # def create_wallets(self, user, wallet_addres_btc="",wallet_addres_dash=""):
    #     wallet = self.create(user=user,wallet_addres_btc=wallet_addres_btc,wallet_addres_dash=wallet_addres_dash)
    #     return wallet
    #
    # def save(self, *args, **kwargs):
    #     super().save(*args, **kwargs)
