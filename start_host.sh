#!/bin/bash

nohup uwsgi --ini ./uwsgi/production.ini --logger file:/home/developer/data/isida/uwsgi.log > /dev/null 2>&1 &
