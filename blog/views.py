import time, json, requests
from django.shortcuts import render, HttpResponse
from django.http import HttpResponseRedirect, JsonResponse
from django.utils.translation import activate
"""import modules"""
# import datetime
# Create your views here.

def blog(request):
    return render(request,'blog/blog_index.html')
