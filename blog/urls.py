from django.conf.urls import url, include
from blog import views
from django.views.generic import RedirectView


urlpatterns = [
    url(r'^blog/$', views.blog, name='blog'),

]
