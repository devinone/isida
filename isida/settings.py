"""
Django settings for isida project.

Generated by 'django-admin startproject' using Django 2.0.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""
import os
import datetime, pytz

from django.utils.translation import ugettext_lazy as _
from datetime import datetime, timedelta
from django.utils.timezone import utc



BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEMPLATE_DIR = os.path.join(BASE_DIR,'templates')###OWN
STATICFILE_DIR = os.path.join(BASE_DIR,'static')###OWN

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')###OWN need to on when collecstatic

SECRET_KEY = os.getenv('SECRET_KEY')



DEBUG = False
###
CSRF_COOKIE_SECURE = True
X_FRAME_OPTIONS = 'DENY'
SECURE_SSL_REDIRECT = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SESSION_COOKIE_SECURE = True

ALLOWED_HOSTS = ['127.0.0.1','89.223.25.199','owlpull.com','www.owlpull.com',]


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'registration',
    'home',
    'requests',
    'widget_tweaks',
    'api',
    'wallet',
    'blog',
    'coin',
]

MIDDLEWARE = [
    'django.middleware.cache.UpdateCacheMiddleware',
    ###
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ####
    'django.middleware.cache.FetchFromCacheMiddleware',
]


ROOT_URLCONF = 'isida.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS':  [TEMPLATE_DIR,],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
            ],
        },
    },
]

WSGI_APPLICATION = 'isida.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '/var/tmp/django_cache',
        'TIMEOUT':60,
    }
}
CACHE_MIDDLEWARE_SECONDS = 60

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/


TIME_ZONE = 'UTC'

USE_I18N = True  # активация системы перевода django

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'###OWN
MEDIA_ROOT = os.path.join(BASE_DIR,'media')###OWN
MEDIA_URL = '/media/'###OWN

STATICFILES_DIRS = [ ###OWN
    STATICFILE_DIR,
]

LOGIN_REDIRECT_URL = '/profile/'

# print("print EMAIL_USE_TLS")
# print(os.getenv('EMAIL_HOST'))
EMAIL_USE_TLS = True
EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD')
EMAIL_PORT = os.getenv('EMAIL_PORT')


DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
#EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_BACKEND = 'django_smtp_ssl.SSLEmailBackend'
AUTHENTICATION_BACKENDS = ('isida.backend.CustomLogin',)
PASSWORD_RESET_TIMEOUT_DAYS=1
ACCOUNT_ACTIVATION_DAYS = 1

LANGUAGES = (
    ('en', _('English')),
    ('ru', _('Russian')),
    # ('zh-cn',_('Chinese')),
)

# месторасположение файлов перевода
LOCALE_PATHS = (
    'locale',
    # os.path.join(PROJECT_DIR, 'locale'),
)

LANGUAGE_CODE = 'en'

IS_START = 0
# print(LANGUAGE_CODE)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/home/developer/data/isida/isida.log',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.template': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}
