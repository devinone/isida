from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import render


def handler403(request,exception, template_name='errors/403.html'):
# def handler403(request):
    # response = render_to_response('errors/403.html')
    # response.status_code = 403
    # return response
    return render(request,'errors/403.html')

def handler404(request,exception, template_name='errors/404.html'):
# def handler404(request):
    response = render_to_response('errors/404.html')
    response.status_code = 404
    return response
    # return render(request,'errors/404.html')


def handler500(request, template_name='errors/500.html'):
# def handler500(request):
    # response = render_to_response('errors/500.html')
    # response.status_code = 500
    # return response
    return render(request,'errors/500.html')


def handler503(request, template_name='errors/503.html'):
# def handler503(request):
    # print(exception)
    # print(request)
    # response = render_to_response('errors/503.html')
    # response.status_code = 503
    # return response
    return render(request,'errors/503.html')
