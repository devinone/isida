"""isida URL Configuration"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
# from sitemaps.views import sitemap
from home import views as home_views
from django.conf.urls.static import static
from django.conf import settings



urlpatterns = [
    url(r'^(?P<filename>(robots.txt)|(humans.txt))$',
        home_views.home_files, name='home-files'),
    # url(r'^sitemap\.xml$', sitemap, name='sitemap-xml'),
]  + static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

urlpatterns += i18n_patterns(
    url(r'^9f34f213f2/f23f23e', admin.site.urls),
    url(r'^', include('home.urls')),
    url(r'^', include('registration.urls')),
    url(r'^', include('api.urls')),
    url(r'^', include('wallet.urls')),
    url(r'^', include('blog.urls')),


)  + static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

handler403 = 'isida.views.handler403'
handler404 = 'isida.views.handler404'
handler500 = 'isida.views.handler500'
handler503 = 'isida.views.handler503'
